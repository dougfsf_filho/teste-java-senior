# teste-java-senior

##Teste de Java Senior

Contrua uma aplicação REST em Java usando Spring Framework que atenda aos seguintes prerrequisitos:

(1) A aplicação recebe e responde às requições no formato JSON
(2) A aplicação deverá ter conexão com um banco de dados (MySQL ou H2 Database) e deve expor endpoints para operar o banco de dados de forma que seja possível:
```
Inserir um Restaurante (nome e endereço)
Exibir a lista de Restaurantes cadastrados
Inserir uma Nota para o restaurante com o nome do autor da nota
Exibir as Notas de um determinado restaurante com sua devida classificação (nota média)
```
(3) (teste para FullStack) Se possível, realizar a construção de uma aplicação de front-end (HTML, CSS, Javascript, JQuery, AngularJS, Angular ou ReactJS) que acesse os endpoints da aplicação e mostre (em sua interface) os dados de restaurantes e classificações, assim como permite (através de formulários) cadastrar restaurantes e notas

##Prerrequisitos

Sua aplicação deve ser compilada e executada via maven ou gradle, deverá ter documentação embedada utilizando Swagger 2 e deve ter testes que atendam aos principios de TDD e BDD. A duração máxima deste teste é de 50 minutos e o mesmo é acompanhado pelo seu recrutador técnico, o quel estará livre para fazer perguntas sobre qual metodologia e qual linha de raciocínio foi abordada durante a construção da aplicação.